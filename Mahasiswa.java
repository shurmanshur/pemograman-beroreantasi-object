package mahasiswa;
import java.util.Date;
import java.util.Scanner;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Scanner;

public class Mahasiswa {
	String nim;
	String nama;
	Double ipk;
	int sks;
	String tglLahir;//formatnya yyyy-mm-dd
	
	public Mahasiswa(String nim, String nama, Double ipk, int sks, String tglLahir) {
		this.nim = nim;
		this.nama = nama;
		this.ipk = ipk;
		this.sks = sks;
		this.tglLahir = tglLahir;
	}
	
	public String getProgdi(String progdi) {
		String ps="";
		if(progdi.equals("A11")) {
			ps = "Teknik Informatika";
		}
		else if(progdi.equals("A12")) {
			ps = "Sistem Informasi";
		}
		else if(progdi.equals("B11")) {
			ps = "Manajemen";
		}
		else if(progdi.equals("B12")) {
			ps = "Akuntansi";
		}
		else {
			ps = "Belum terdaftar";
		}
		
		return ps;
	}
	
	public String ipkStatus() {
		String stat="";
		if (ipk <= 1 && ipk > 0) {
			stat = "Perlu  usaha lebih";
		}
		else if(ipk > 1 && ipk <= 2) {
			stat = "Perlu segera menaikkan IPK";
		}
		else if(ipk > 2 && ipk <= 2.75) {
			stat = "Sedikit lafi";
		}
		else if(ipk > 2.75 && ipk <= 3) {
			stat = "Memmuaskan";
		}
		else if(ipk > 3 && ipk <= 3.5) {
			stat = "Sangat Memuaskan";
		}
		else if(ipk > 3.5 && ipk <= 4) {
			stat = "Dengan pujian";
		}
		else {
			stat = "Range ipk di luar jalur";
		}
		/**
		 * jika 0 < ipk <=1 , maka stat = "Perlu usaha lebih"
		 * jika 1 < ipk <=2, maka stat = "Perlu segera menaikkan IPK"
		 * jika 2 < ipk <=2.75, maka stat = "Sedikit lagi"
		 * jika 2.75 < ipk <= 3, maka stat = "Memuaskan"
		 * jika 3 < ipk <= 3.5, maka stat = "Sangat Memuaskan
		 * jika 3.5 < ipk <=4, maka stat = "Dengan pujian"
		 * jika di atas 4, maka stat = "Range ipk di luar jalur" 
		 * 
		 * */
		
		
		return stat;
	}
	
	public int getTahun() {
		int angkatan;
		angkatan = 0;
		Mahasiswa mhs = new Mahasiswa(nim, nama, ipk, sks, tglLahir);
		String prodi2 = mhs.nim;
		String prrodi3 = prodi2.substring(4,8);
		int tahun = Integer.parseInt(prrodi3);
		
		angkatan = tahun;
		
		/**
		 * cari angkatan dari nim yang diinput
		 * misal A11.2000.00001, maka akan mengembalikan 2000
		 * 
		 */
		return angkatan;
	}
	
	public int getTagihanSks() {
		int perSks = 250000;
		int tagihan = 0;
		
		tagihan = perSks * sks;
		/**
		 * cari berapa jumlah tagihan mahasiswa yang bersangkutan
		 * sks*perSks
		 */
		
		return tagihan;
	}
	
	public int getMhsSemester() {
		int smt = 0;
		Calendar kld = Calendar.getInstance();
		int thnSkr = kld.get(Calendar.YEAR);
		
		// System.out.println(thnSkr); = (2022)
		Mahasiswa mhs = new Mahasiswa(nim, nama, ipk, sks, tglLahir);
		int imt = thnSkr - mhs.getTahun();
		smt = (imt * 12) / 6;
		/**
		 * Hitung mahasiswa sudah berapa semester kuliah
		 */
		
		return smt;
	}
	
	private Date dateFormatter(String pola, String tanggal) {
		Date tgl=null;
		SimpleDateFormat formatter = new SimpleDateFormat(pola);
		
		try {
			tgl = formatter.parse(tanggal);
		}
		catch(ParseException e) {
			e.printStackTrace();
		}
		return tgl;
	}
	
	Date DISPLAY1 (String Lala, String Popo) {
		Mahasiswa maha = new Mahasiswa(nim, nama, ipk, sks, tglLahir);
		String twtw = Lala;
		maha.dateFormatter(twtw, Popo);
		System.out.println(maha.dateFormatter(twtw, Popo));
		return null;
	}
	
	public String getUmur() {
		String umur = "";		
		
		/**
		 * Hitung umur di sini
		 */
		
		return umur;
		
	}
}
